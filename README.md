# AmongUs Stats 👨🏼‍🚀
<img src="https://workspace.occhipinti.dev/uploaded/image-20210106181027365.png" alt="alt text" width="900" height="whatever">

AmongUs Stats è un sistema che permette di tenere traccia delle statistiche tue e dei tuoi amici durante le vostre partite di AmongUs. 

## Getting Started 🚀

Per avviare il webserver basta clonare il progetto dalla repo di GitHub, installare le 
dipendenze del progetto tramite maven con il comando `mvn install` e lanciare il comando maven `mvn spring-boot:run`.
Un utente demo per provare il sistema è `admin@admin.com:admin` e il pannello di amministrazione è disponibile all'indirizzo [http://localhost:8080/admin](http://localhost:8080/admin) (visitabile dopo il login).  

## Utenti demo 🧐

|   email| password  | admin  | 
|---|---|---|
|  admin@admin.com  | admin  | true  |  
| gianlo@ass3.dev  | gianlo  | false  |   
| andrea@ass3.dev  | andrea  | false |  
| davide@ass3.dev  | davide  | false |  

Dopo aver effettuato il login è possibile accedere al pannello di amministrazione dall'indirizzo [http://localhost:8080/admin](http://localhost:8080/admin).
### Pre-requisiti 📋

AmongUs Stats è stato sviluppato con il **JDK 14**.

## Features 📖

- Puoi tenere traccia delle partite giocate su AmongUs
- Puoi aggiungere dei tuoi amici alle partite senza che si siano registrati (si registreranno in seguito)
- Non c'è un owner di una partia, il controllo è decentralizzato (senza considerare gli admin). Se tutti si cancellano da una partita la partita in automatico la partita si cancella.
- Puoi modificare solo le statistiche e i ruoli a te associati (sistema di permessi embedded)
- Solo gli admin possono aggiungere mappe e task
- C'è un limite di persone che puoi aggiungere ad una partita, settabile quando crei la partita
- Puoi ricercare le partite per data, codice ed email (di un player presente all'interno della partita)
- Nella vista delle partite vedi immediatamente quali partite hai giocato da impostore (evidenziate in rosso) e quali partite hai giocato da crewmate (evidenziate di blu)
- Solo gli amministratori possono modificare partite e ruoli di tutti


## Autori ✒️

* **Gianlorenzo Occhipinti** - *829524*
* **Davide Piovani** - *830113*
* **Andrea Mariotti** - *829534*
