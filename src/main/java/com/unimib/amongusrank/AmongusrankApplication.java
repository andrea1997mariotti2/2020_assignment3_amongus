package com.unimib.amongusrank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AmongusrankApplication {

    public static void main(String[] args) {
        SpringApplication.run(AmongusrankApplication.class, args);
    }

}
