package com.unimib.amongusrank.service.login;

import com.unimib.amongusrank.model.Player;
import com.unimib.amongusrank.model.Role;
import com.unimib.amongusrank.repository.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Optional;

@Service
@Transactional
public class LoginService {

    @Autowired
    private PlayerRepository playerRepository;


    /**
     * Check if credentials are correct
     */
    public boolean isLoginCorrect(String email, String password){
        if(playerRepository.isCredentialCorrect(email, password).isEmpty())
            return false;
        return true;
    }

    /**
     * Check if email already exist
     */
    public boolean isEmailAlreadyExisting(String email){
        return !playerRepository.isEmailExisting(email).isEmpty();
    }

    /**
     * Setup the email password
     */
    public void modifyPasswordByEmail(String email, String password){
        playerRepository.setPasswordByEmail(email,password);
    }

    /**
     * Add a new player to DB
     */
    public void addPlayer(Player player){
        playerRepository.save(player);
    }

    /**
     * Edit player with new details
     */
    public void editPlayer(Player newPlayerData){
        Optional<Player> player = playerRepository.getPlayerById(newPlayerData.getId());

        if(player.isPresent()){

            Player playerToBeSaved = player.get();

            if(!newPlayerData.getEmail().equals("")){
                playerToBeSaved.setEmail(newPlayerData.getEmail());
            }

            System.out.println(newPlayerData.getPassword());

            if(!newPlayerData.getPassword().equals("")){
                playerToBeSaved.setPassword(newPlayerData.getPassword());
            }

            playerRepository.save(playerToBeSaved);
        }
    }

    /**
     * Deny access to player
     */
    public void deletePlayer(long playerId){
        Optional<Player> player = playerRepository.getPlayerById(playerId);

        if(player.isPresent()){

            Player playerToBeSaved = player.get();

            playerToBeSaved.setPassword(null);

            playerRepository.save(playerToBeSaved);
        }
    }

    /**
     * Get playerId by email
     */
    public Long getIdByEmail(String email) {return playerRepository.getPlayerByEmail(email).getId(); };

    /**
     * Register new player
     */
    public Player registerOrGetByEmail(String email){
        if (isEmailAlreadyExisting(email)){
            return playerRepository.getPlayerByEmail(email);
        } else {
           Player player = new Player();
           player.setEmail(email);
           addPlayer(player);
           return player;
        }
    }

    /**
     * Get player by id
     */
    public Optional<Player> getPlayerById(long id){
        return playerRepository.findById(id);
    }

    /**
     * Check if the playerId is associated with an admin id
     */
    public boolean checkIfPlayerIsAdmin(long playerId){
        Optional<Player> player = getPlayerById(playerId);
        return player.isPresent() && player.get().isAdmin();
    }

}
