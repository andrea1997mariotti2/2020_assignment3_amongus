package com.unimib.amongusrank.service.admin;

import com.unimib.amongusrank.model.*;
import com.unimib.amongusrank.repository.TaskRepository;
import com.unimib.amongusrank.service.game.role.task.CrewmateTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class TaskService {

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private CrewmateTaskService crewmateTaskService;

    /**
     * Get the list of all tasks
     */
    public Iterable<Task> getAllTasks() {
        return taskRepository.findAll();
    }

    /**
     * Save a new task on DB
     */
    public void saveTask(Task task){
        taskRepository.save(task);
    }

    /**
     * Get one task by id
     */
    public Optional<Task> getTaskById(long id) {
        return taskRepository.findById(id);
    }

    /**
     * Remove one task by id
     */
    public void removeTask(long id){
        Optional<Task> task = getTaskById(id);
        task.ifPresent(value -> {
            crewmateTaskService.removeCrewmateTaskOnTask(value);
            taskRepository.delete(value);
        });
    }


}


