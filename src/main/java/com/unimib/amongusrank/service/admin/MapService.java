package com.unimib.amongusrank.service.admin;

import com.unimib.amongusrank.model.Map;
import com.unimib.amongusrank.repository.MapRepository;
import com.unimib.amongusrank.service.game.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
@Transactional
public class MapService {

    @Autowired
    private MapRepository mapRepository;

    @Autowired
    private GameService gameService;

    /**
     * Get the list of all maps
     */
    public Iterable<Map> getMaps(){
        return mapRepository.findAll();
    }

    /**
     * Save a map on DB
     */
    public void saveMap(Map map){
        mapRepository.save(map);
    }

    /**
     * Get one map by id
     */
    public Optional<Map> getMapById(long id) {
        return mapRepository.findById(id);
    }

    /**
     * Remove one map by id
     */
    public void removeMap(long id){
        Optional<Map> map = getMapById(id);
        map.ifPresent(value ->{
            gameService.removeAllGamesOnMap(value);
            mapRepository.delete(value);
        });
    }


}