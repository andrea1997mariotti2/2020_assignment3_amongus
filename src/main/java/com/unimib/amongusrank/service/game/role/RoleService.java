package com.unimib.amongusrank.service.game.role;

import com.fasterxml.jackson.annotation.JacksonAnnotationsInside;
import com.unimib.amongusrank.controller.game.AdditionalFormOptions;
import com.unimib.amongusrank.model.Crewmate;
import com.unimib.amongusrank.model.Game;
import com.unimib.amongusrank.model.Player;
import com.unimib.amongusrank.model.Role;
import com.unimib.amongusrank.repository.CrewmateRepository;
import com.unimib.amongusrank.repository.RoleRepository;
import com.unimib.amongusrank.service.game.GameService;
import com.unimib.amongusrank.service.game.role.task.CrewmateTaskService;
import com.unimib.amongusrank.service.game.role.vote.VoteService;
import com.unimib.amongusrank.service.login.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Optional;

/*
    beans hold the business logic and call methods in the repository layer.
 */

@Service
@Transactional
public class RoleService {

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private GameService gameService;

    @Autowired
    private LoginService loginService;

    @Autowired
    private VoteService voteService;

    @Autowired
    private CrewmateService crewmateService;

    @Autowired
    private ImpostorService impostorService;



    /**
     * Save role on DB
     */
    public void saveRole(Role role){
        roleRepository.save(role);
    }

    /**
     * Get role by roleId
     */
    public Optional<Role> getRoleById(Long roleId){
        return roleRepository.findById(roleId);
    }

    /**
     * Get all roles associated to a PlayerId
     */
    public Iterable<Role> getRolesByPlayerId(long id){
        Optional<Player> player = loginService.getPlayerById(id);

        return player.isPresent() ?
                roleRepository.getRoleByPlayer(player.get()) :
                new ArrayList();
    }

    /**
     * Get all roles inside a game
     * */
    public Iterable<Role> getRolesByGame(Game game ){
        return roleRepository.getRoleByGame(game);
    }

    /**
     * Add a game role built with some additional options (useful for form without pojo)
     * */
    public void addGameRoleWithOptions(Role role, AdditionalFormOptions options, String gameId){
        Game game = gameService.getGameById(Long.parseLong(gameId)).get();
        Player player = loginService.registerOrGetByEmail(options.getEmail());

        role.setAlive(options.isAlive());
        role.setGame(game);
        role.setPlayer(player);
        saveRole(role);
    }

    /**
     * Check if role is associated with a playerId
     */
    public boolean verifyRolePayerID(String playerId, Role role){
        long longId =Long.parseLong(playerId);
        return  role.getPlayer().getId() == longId;
    }

    public void removeAllRolesOnGame(Game game){
        getRolesByGame(game).forEach(role -> {
            voteService.removeVoteOnRole(role);
            if(role instanceof Crewmate){
                crewmateService.removeCrewmateById(role.getId());
            }else{
                impostorService.removeImpostorById(role.getId());
            }
            roleRepository.delete(role);
        });
    }
}
