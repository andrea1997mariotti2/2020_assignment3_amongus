package com.unimib.amongusrank.service.game;

import com.unimib.amongusrank.model.*;
import com.unimib.amongusrank.model.Map;
import com.unimib.amongusrank.repository.GameRepository;
import com.unimib.amongusrank.service.game.role.RoleService;
import com.unimib.amongusrank.service.login.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.StreamSupport;

@Service
@Transactional
public class GameService {

    @Autowired
    private GameRepository gameRepository;

    @Autowired
    private RoleService roleService;

    /**
     * Get all games
     */
    public Iterable<Game> getGames(){
        return gameRepository.findAll();
    }

    /**
     * Save a game on DB
     */
    public void saveGame(Game game){
        gameRepository.save(game);
    }

    /**
     * search game by code or by data
     */
    public Iterable<Game> searchGame(String code, String data, String email) throws ParseException {
        Date dataa=null;
        if(!data.equals(""))
             dataa = new SimpleDateFormat( "yyyy-MM-dd").parse(data);
      //  System.out.println("\n\n\n code: "+code+ "data: "+data+"\n\n\n");
        code = code.equals("") ? null : code;
        email = email.equals("") ? null : email;

        return gameRepository.searchGameEmail(code,dataa,email);
    }

    /**
     * Get game by id
     */
    public Optional<Game> getGameById(long gameId){
        return gameRepository.getGameById(gameId);
    }

    /**
     * Check if game is empty, if so delete it
     */
    public void deleteGameIfEmpty(Game game){
        if(StreamSupport.stream(roleService.getRolesByGame(game).spliterator(), false).count() == 0){
            gameRepository.delete(game);
        }
    }

    /**
     * Get the number of roles into a game
     */
    public int getGameRolesCount(Game game){
        Iterable<Role> gameRoles = roleService.getRolesByGame(game);
        Iterator<Role> gameIterator = gameRoles.iterator();
        int count = 0;
        while (gameIterator.hasNext()){
            gameIterator.next();
            count++;
        }
        return count;
    }

    /**
     * Remove all games on map
     */
    public void removeAllGamesOnMap(Map map){
        gameRepository.getGamesByMap(map).forEach(game -> {
            roleService.removeAllRolesOnGame(game);

        });


    }
}
