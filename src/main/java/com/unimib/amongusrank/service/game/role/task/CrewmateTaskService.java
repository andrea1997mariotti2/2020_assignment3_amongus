package com.unimib.amongusrank.service.game.role.task;


import com.unimib.amongusrank.model.Crewmate;
import com.unimib.amongusrank.model.CrewmateTask;
import com.unimib.amongusrank.model.Task;
import com.unimib.amongusrank.repository.CrewmateTaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
@Transactional
public class CrewmateTaskService {

    @Autowired
    CrewmateTaskRepository crewmateTaskRepository;


    /**
     * Get crewmateTasks by crewmate
     */
    public Iterable<CrewmateTask> getAllCrewmateTasks(Crewmate crewmate) {
        return crewmateTaskRepository.getCrewmateTaskByCrewmate(crewmate);
    }

    /**
     * Remove all tasks associated to one task
     */
    public void removeCrewmateTaskOnTask(Task task) {
        crewmateTaskRepository.deleteAll(crewmateTaskRepository.getCrewmateTaskByTask(task));
    }

    /**
     * Remove all tasks associated to one crewmate
     */
    public void removeCrewmateTaskOnCrewmate(Crewmate crewmate) {
        crewmateTaskRepository.deleteAll(crewmateTaskRepository.getCrewmateTaskByCrewmate(crewmate));
    }

    /**
     * Save crewmateTask on DB
     */
    public void saveCTask(CrewmateTask crewmateTask) {
        crewmateTaskRepository.save(crewmateTask);
    }

    /**
     * Delete crewmateTask by crewmate and task
     */
    public void removeCrewmateTask(long taskId) {
        Optional<CrewmateTask> crewmateTask = crewmateTaskRepository.findById(taskId);
        crewmateTask.ifPresent(value -> crewmateTaskRepository.delete(value));
    }


}
