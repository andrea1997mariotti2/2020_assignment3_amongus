package com.unimib.amongusrank.service.game.role;

import com.unimib.amongusrank.model.*;
import com.unimib.amongusrank.repository.ImpostorRepository;
import com.unimib.amongusrank.service.game.GameService;
import com.unimib.amongusrank.service.game.role.vote.VoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/*
    beans hold the business logic and call methods in the repository layer.
 */

@Service
@Transactional
public class ImpostorService {

    @Autowired
    private ImpostorRepository impostorRepository;

    @Autowired
    GameService gameService;

    @Autowired
    RoleService roleService;

    @Autowired
    VoteService voteService;

    /**
     * Get impostor by id
     */
    public Optional<Impostor> getImpostorById(Long id){
        return impostorRepository.findById(id);
    }

    /**
     * Remove impostor by id
     */
    public void removeImpostorById(Long id){
        Optional<Impostor> impostor = impostorRepository.findById(id);

        if(impostor.isPresent()){
            voteService.removeVoteOnRole(impostor.get());
            impostorRepository.delete(impostor.get());
            gameService.deleteGameIfEmpty(impostor.get().getGame());
        }

    }

    /**
     * Ckeck if roleId is associated with an impostor
     */
    public boolean isImpostor(String roleId){
        long id = Long.parseLong(roleId);
        return getImpostorById(id).isPresent();
    }

    /**
     * Get all impostors on specific game
     */
    public List<Impostor> getImpostorsOnGame(Game game){
        List<Impostor> impostors = new ArrayList<>();

        roleService.getRolesByGame(game).forEach(role -> {
            if(role instanceof Impostor){
                impostors.add((Impostor) role);
            }
        });

        return impostors;
    }


}
