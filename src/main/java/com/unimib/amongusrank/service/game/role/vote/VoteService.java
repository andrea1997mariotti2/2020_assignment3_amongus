package com.unimib.amongusrank.service.game.role.vote;

import com.unimib.amongusrank.model.Game;
import com.unimib.amongusrank.model.Map;
import com.unimib.amongusrank.model.Role;
import com.unimib.amongusrank.model.Vote;
import com.unimib.amongusrank.repository.VoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;


@Service
@Transactional
public class VoteService {

    @Autowired
    private VoteRepository voteRepository;

    /**
     * Get votes given by a Role
     */
    public  Iterable<Vote> getVotesByRole(Role role){
        return voteRepository.findAllByVoteFrom(role);
    }

    /**
     * Get votes given to a Role
     */
    public Iterable<Vote> getVotesOnRole(Role role){
        return voteRepository.findAllByVoteTo(role);
    }

    /**
     * Remove all votes associated to one Role
     */
    public  void removeVoteOnRole(Role role){
       getVotesOnRole(role).forEach(vote -> voteRepository.delete(vote));
       getVotesByRole(role).forEach(vote -> voteRepository.delete(vote));
    }

    /**
     * Add a vote
     */
    public void addVote(Vote vote){
        voteRepository.save(vote);
    }

    /**
     * Remove one vote by voteId
     */
    public void removeVoteById(long voteId){
        Optional<Vote> vote = voteRepository.findById(voteId);
        vote.ifPresent(value -> voteRepository.delete(value));
    }

}