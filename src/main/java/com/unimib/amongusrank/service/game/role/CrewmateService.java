package com.unimib.amongusrank.service.game.role;


import com.unimib.amongusrank.model.Crewmate;
import com.unimib.amongusrank.model.Game;
import com.unimib.amongusrank.model.Task;
import com.unimib.amongusrank.repository.CrewmateRepository;
import com.unimib.amongusrank.service.admin.TaskService;
import com.unimib.amongusrank.service.game.GameService;
import com.unimib.amongusrank.service.game.role.task.CrewmateTaskService;
import com.unimib.amongusrank.service.game.role.vote.VoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class CrewmateService {

    @Autowired
    private CrewmateRepository crewmateRepository;

    @Autowired
    GameService gameService;

    @Autowired
    TaskService taskService;

    @Autowired
    CrewmateTaskService crewmateTaskService;

    @Autowired
    RoleService roleService;

    @Autowired
    VoteService voteService;

    /**
     * Get crewmate by id
     */
    public Optional<Crewmate> getCrewMateById(long id){
        return crewmateRepository.findById(id);
    }

    /**
     * Remove crewmate by id
     */
    public void removeCrewmateById(Long id){
        Optional<Crewmate> crewmate = crewmateRepository.findById(id);

        if(crewmate.isPresent()){
            voteService.removeVoteOnRole(crewmate.get());
            crewmateTaskService.removeCrewmateTaskOnCrewmate(crewmate.get());
            crewmateRepository.delete(crewmate.get());
            gameService.deleteGameIfEmpty(crewmate.get().getGame());
        }
    }


    /**
     * Get all crewmates on specific game
     */
    public List<Crewmate> getCrewmatesOnGame(Game game){
        List<Crewmate> crewmates = new ArrayList<>();

        roleService.getRolesByGame(game).forEach(role -> {
            if(role instanceof Crewmate){
                crewmates.add((Crewmate) role);
            }
        });

        return crewmates;
    }


}
