package com.unimib.amongusrank.repository;

import com.unimib.amongusrank.model.*;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.Iterator;
import java.util.Optional;


@Repository
public interface VoteRepository extends CrudRepository<Vote,Long> {

    @Query("from Vote v where v.voteFrom = :voting_id")
    Iterable<Vote> findAllByVoting(@Param("voting_id") long voting_id);

    Iterable<Vote> findAllByVoteFrom(Role role);

    Iterable<Vote> findAllByVoteTo(Role role);


}
