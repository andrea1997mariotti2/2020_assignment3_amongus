package com.unimib.amongusrank.repository;

import com.unimib.amongusrank.model.Game;
import com.unimib.amongusrank.model.Map;
import com.unimib.amongusrank.model.Player;
import com.unimib.amongusrank.model.Task;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

@Repository
public interface GameRepository extends CrudRepository<Game, Long>{

    //@Query("FROM Task t where t.taskName like %:nomeTask%")
    //public List<Task> taskpernome(@Param("nomeTask")String nomeTask);
    @Query("FROM Game g where g.code like :codice OR g.date = :data OR g.code = :email")
    public Iterable<Game> searchGame(@Param("codice") String codice, @Param("data") Date data, @Param("email") String email);

    public Optional<Game> getGameById(long id);

    @Query("FROM Game g " +
            "where COALESCE( :codice,g.code) like g.code " +
            "AND COALESCE( :data, g.date) = g.date " +
            "AND g.id in (SELECT g.id " +
                            "FROM Game g, Player p, Role r " +
                            "where COALESCE(:email, r.player.email ) = r.player.email  AND r.game.id = g.id )")
    public Iterable<Game> searchGameEmail(@Param("codice") String codice, @Param("data") Date data, @Param("email") String email);

    @Query("select g.id FROM Game g, Player p, Role r where r.player.email = :email AND r.game.id = g.id")
    public Iterable<Game> emmail( @Param("email") String email);

    public Iterable<Game> getGamesByMap(Map map);

}
