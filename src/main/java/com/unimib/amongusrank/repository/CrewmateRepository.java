package com.unimib.amongusrank.repository;

import com.unimib.amongusrank.model.Crewmate;
import com.unimib.amongusrank.model.Game;
import com.unimib.amongusrank.model.Player;
import com.unimib.amongusrank.model.Role;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

/*
    This is to indicate that the class defines a data repository.
 */

@Repository
public interface CrewmateRepository extends CrudRepository<Crewmate, Long> {



}
