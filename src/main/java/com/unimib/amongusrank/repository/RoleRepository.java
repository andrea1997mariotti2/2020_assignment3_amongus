package com.unimib.amongusrank.repository;

import com.unimib.amongusrank.model.Game;
import com.unimib.amongusrank.model.Player;
import com.unimib.amongusrank.model.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


/*
    This is to indicate that the class defines a data repository.
 */

@Repository
public interface RoleRepository extends CrudRepository<Role, Long> {

     Iterable<Role> getRoleByGame(Game game);

     Iterable<Role> getRoleByPlayer(Player player);


}
