package com.unimib.amongusrank.repository;


import com.unimib.amongusrank.model.Impostor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ImpostorRepository  extends CrudRepository<Impostor, Long> {



}
