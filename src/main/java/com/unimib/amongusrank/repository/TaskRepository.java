package com.unimib.amongusrank.repository;

import com.unimib.amongusrank.model.Player;
import com.unimib.amongusrank.model.Task;
import com.unimib.amongusrank.model.Vote;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TaskRepository extends CrudRepository<Task, Long>{


}
