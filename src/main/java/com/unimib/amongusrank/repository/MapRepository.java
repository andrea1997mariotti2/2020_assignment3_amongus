package com.unimib.amongusrank.repository;

import com.unimib.amongusrank.model.Map;
import com.unimib.amongusrank.model.Player;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MapRepository extends CrudRepository<Map, Long> {

}
