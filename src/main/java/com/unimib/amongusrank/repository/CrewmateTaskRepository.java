package com.unimib.amongusrank.repository;

import com.unimib.amongusrank.model.*;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CrewmateTaskRepository extends CrudRepository<CrewmateTask, Long> {

    public Iterable<CrewmateTask> getCrewmateTaskByCrewmate(Crewmate crewmate);

    public Iterable<CrewmateTask> getCrewmateTaskByTask(Task task);

}
