package com.unimib.amongusrank.repository;

import com.unimib.amongusrank.model.Player;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface PlayerRepository extends CrudRepository<Player, Long> {

    @Query("FROM Player u where u.email = :emailAddress and u.password = :password")
    List<Player> isCredentialCorrect(@Param("emailAddress") String emailAddress, @Param("password") String password);

    @Query("FROM Player u where u.email = :email")
    List<Player> isEmailExisting(@Param("email") String email);

    @Modifying
    @Query("update Player p set p.password = :password where p.email = :email")
    void setPasswordByEmail(@Param("email") String email, @Param("password") String password);

    @Query("from Player p where p.email = :email")
    Player getPlayerByEmail(@Param("email") String email);

    Optional<Player> getPlayerById(long playerId);

}
