package com.unimib.amongusrank.controller.login;

import com.unimib.amongusrank.model.Player;
import com.unimib.amongusrank.service.login.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import java.util.Optional;

import static com.unimib.amongusrank.controller.ControllerEndpoint.*;

@Controller
@RequestMapping(LOGIN_ENDPOINT)
public class LoginController {

    @Autowired
    private LoginService service;

    @GetMapping("")
    public ModelAndView index(@CookieValue(name = "loggedPlayer", defaultValue = "-1") String loggedPlayer){
        ModelAndView modelAndView = new ModelAndView();
        if(!loggedPlayer.equals("-1")){
            //Player is logged in

            modelAndView.setViewName("redirect:" + GAME_ENDPOINT);
        }else{
            //Player is not logged in

            modelAndView.setViewName("login/login.html");
            modelAndView.addObject("player", new Player());
        }

        return modelAndView;
    }

    @PostMapping("/controldata")
    public ModelAndView loginPlayer(
            @ModelAttribute Player player,
            HttpServletResponse response){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("player", new Player());

        if(!service.isLoginCorrect(player.getEmail(), player.getPassword())) {
            modelAndView.setViewName("login/login.html");
            modelAndView.addObject("error", true);
        }else{
            player.setId(service.getIdByEmail(player.getEmail()));
            modelAndView.setViewName("redirect:" + GAME_ENDPOINT);

            //Add the cookie
            Cookie cookie = new Cookie("loggedPlayer", String.valueOf(player.getId()));
            cookie.setPath("/");
            response.addCookie(cookie);
        }
        return modelAndView;
    }


    @GetMapping("/editPlayer")
    public ModelAndView modificaProfilo(@CookieValue(name = "loggedPlayer", defaultValue = "-1") String loggedPlayer){
        ModelAndView modelAndView = new ModelAndView();

        Optional<Player> player = service.getPlayerById(Long.parseLong(loggedPlayer));
        if(player.isPresent()){
            modelAndView.addObject("player", player.get());
            modelAndView.setViewName("login/editPlayer");
        }else{
            modelAndView.setViewName("redirect:" + LOGIN_ENDPOINT);
        }

        return modelAndView;
    }

    @GetMapping("/deletePlayer")
    public ModelAndView deletePlayer(
            @CookieValue(name = "loggedPlayer", defaultValue = "-1") String loggedPlayer,
            HttpServletResponse response){
        ModelAndView modelAndView = new ModelAndView();

        service.deletePlayer(Long.parseLong(loggedPlayer));

        //Delete the cookie
        Cookie cookie = new Cookie("loggedPlayer", "-1");
        cookie.setPath("/");
        cookie.setMaxAge(0);
        response.addCookie(cookie);

        modelAndView.setViewName("redirect:" + LOGIN_ENDPOINT);
        return modelAndView;
    }

    @PostMapping("/editPlayer")
    public ModelAndView modificaPassword(@CookieValue(name = "loggedPlayer", defaultValue = "-1") String loggedPlayer,
                                         @ModelAttribute Player player){
        ModelAndView modelAndView = new ModelAndView();

        player.setId(Long.parseLong(loggedPlayer)); //Form withoud id
        service.editPlayer(player);

        modelAndView.setViewName("redirect:" + LOGIN_ENDPOINT);
        return modelAndView;
    }


}
