package com.unimib.amongusrank.controller.admin;

import com.unimib.amongusrank.model.Player;
import com.unimib.amongusrank.model.Task;
import com.unimib.amongusrank.service.admin.TaskService;
import com.unimib.amongusrank.service.login.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Optional;

import static com.unimib.amongusrank.controller.ControllerEndpoint.*;

@Controller
@RequestMapping(ADMIN_TASK_ENDPOINT)
public class TaskController{

    @Autowired
    private TaskService service;

    @Autowired
    private LoginService adminService;


    /*  CREATE */
    @GetMapping("/add")
    public ModelAndView addTask() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("task", new Task());
        modelAndView.setViewName("admin/task/task");

        return modelAndView;
    }

    /*  UPDATE   */
    @GetMapping("/update/{id}")
    public ModelAndView editTask(@PathVariable(value="id") String id) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("task", service.getTaskById(Long.parseLong(id)).get());
        modelAndView.setViewName("admin/task/task");

        return modelAndView;
    }

    /*  READ  */
    @GetMapping("")
    public ModelAndView listTasks(@CookieValue(name = "loggedPlayer", defaultValue = "-1") String loggedPlayer) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("tasks", service.getAllTasks());

        Optional<Player> player = adminService.getPlayerById(Long.parseLong(loggedPlayer));
        if(player.isPresent() && player.get().isAdmin()){
            modelAndView.setViewName("admin/task/tasks");
        }else {
            modelAndView.setViewName("redirect:" + GAME_ENDPOINT);
        }

        return modelAndView;
    }

    /*  DELETE   */
    @GetMapping("/delete/{id}")
    public ModelAndView removeTask(@PathVariable(value="id") String id) {
        service.removeTask(Long.parseLong(id));
        return new ModelAndView("redirect:" + ADMIN_TASK_ENDPOINT);
    }

    /*  SAVE TASK ON DB */
    @PostMapping("/add")
    public ModelAndView addTask(@ModelAttribute Task task) {
        service.saveTask(task);

        return new ModelAndView("redirect:" + ADMIN_TASK_ENDPOINT);
    }

}
