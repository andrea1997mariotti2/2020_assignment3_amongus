package com.unimib.amongusrank.controller.admin;

import com.unimib.amongusrank.model.Player;
import com.unimib.amongusrank.service.login.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Optional;

import static com.unimib.amongusrank.controller.ControllerEndpoint.ADMIN_ENDPOINT;



@Controller
@RequestMapping(ADMIN_ENDPOINT)
public class AdminController {

    @Autowired
    LoginService loginService;

    @GetMapping("")
    public ModelAndView index(@CookieValue(name = "loggedPlayer", defaultValue = "-1") String loggedPlayer){
        Optional<Player> player = loginService.getPlayerById(Long.parseLong(loggedPlayer));

        ModelAndView modelAndView = new ModelAndView();

        if(player.isPresent() && player.get().isAdmin()){
            modelAndView.setViewName("admin/admin");
        }else{
            modelAndView.setViewName("redirect:/games");
        }

        return modelAndView;
    }

}
