package com.unimib.amongusrank.controller.admin;

import com.unimib.amongusrank.model.Map;
import com.unimib.amongusrank.model.Player;
import com.unimib.amongusrank.service.admin.MapService;
import com.unimib.amongusrank.service.login.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Optional;

import static com.unimib.amongusrank.controller.ControllerEndpoint.*;

@Controller
@RequestMapping(ADMIN_MAP_ENDPOINT)
public class MapController {

    @Autowired
    private MapService service;

    @Autowired
    private LoginService loginService;


    /*  CREATE  */
    @GetMapping("/add")
    public ModelAndView addMap() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("map", new Map());
        modelAndView.setViewName("admin/map/map");

        return modelAndView;
    }

    /*  READ  */
    @GetMapping("")
    public ModelAndView listMaps(@CookieValue(name = "loggedPlayer", defaultValue = "-1") String loggedPlayer) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("maps", service.getMaps());

        Optional<Player> player = loginService.getPlayerById(Long.parseLong(loggedPlayer));
        if (player.isPresent() && player.get().isAdmin()) {
            modelAndView.setViewName("admin/map/maps");
        } else {
            modelAndView.setViewName("redirect:" + GAME_ENDPOINT);
        }

        return modelAndView;
    }

    /*  UPDATE   */
    @GetMapping("/update/{id}")
    public ModelAndView editMap(@PathVariable(value = "id") String id) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("map", service.getMapById(Long.parseLong(id)).get());
        modelAndView.setViewName("admin/map/map");

        return modelAndView;
    }

    /*  DELETE   */
    @GetMapping("/delete/{id}")
    public ModelAndView removeMap(@PathVariable(value = "id") String id) {
        service.removeMap(Long.parseLong(id));
        return new ModelAndView("redirect:" + ADMIN_MAP_ENDPOINT);
    }


    /*  SAVE MAP ON DB */
    @PostMapping("/add")
    public ModelAndView saveMap(@ModelAttribute Map map) {
        service.saveMap(map);
        return new ModelAndView("redirect:" + ADMIN_MAP_ENDPOINT);
    }
}