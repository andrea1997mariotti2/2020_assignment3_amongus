package com.unimib.amongusrank.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import static com.unimib.amongusrank.controller.ControllerEndpoint.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AmongusController {

    @GetMapping("/")
    public ModelAndView indecEndpoint(){
        return new ModelAndView("redirect:" + LOGIN_ENDPOINT);
    }

}
