package com.unimib.amongusrank.controller.game.role;


import com.unimib.amongusrank.controller.game.AdditionalFormOptions;
import com.unimib.amongusrank.model.*;
import com.unimib.amongusrank.service.game.GameService;
import com.unimib.amongusrank.service.game.role.CrewmateService;
import com.unimib.amongusrank.service.game.role.ImpostorService;
import com.unimib.amongusrank.service.game.role.RoleService;
import com.unimib.amongusrank.service.login.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Optional;

import static com.unimib.amongusrank.controller.ControllerEndpoint.*;

@Controller
@RequestMapping(GAME_ROLE_ENDPOINT)
public class GameRolesController {

    @Autowired
    GameService gameService;

    @Autowired
    RoleService roleService;

    @Autowired
    ImpostorService impostorService;

    @Autowired
    LoginService loginService;

    @Autowired
    CrewmateService crewmateService;


    /* Add role to game  */
    @GetMapping("")
    public ModelAndView addRoletToGame(@PathVariable(value="gameId") String gameId) {
        Optional<Game> game = gameService.getGameById(Long.parseLong(gameId));
        ModelAndView modelAndView = new ModelAndView();

        if(game.isPresent()){
            modelAndView.setViewName("game/role/addRole");
            modelAndView.addObject("impostorNumber", impostorService.getImpostorsOnGame(game.get()).size());
            modelAndView.addObject("crewmateNumber", crewmateService.getCrewmatesOnGame(game.get()).size());
        }else {
            modelAndView.setViewName("redirect:" + GAME_ENDPOINT);
        }

        return modelAndView;
    }

    /* Add an impostor to game */
    @GetMapping("/impostor/add")
    public ModelAndView addGameImpostor(@PathVariable(value="gameId") String gameId) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("game/role/addImpostor");
        modelAndView.addObject("impostor", new Impostor());
        modelAndView.addObject("option", new AdditionalFormOptions());
        modelAndView.addObject("gameId", gameId);

        return modelAndView;
    }

    /* Edit an impostor */
    @GetMapping("/impostor/{impostorId}/edit")
    public ModelAndView editGameImpostor(
            @PathVariable(value="gameId") String gameId,
            @PathVariable(value="impostorId") String impostorId) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("game/role/addImpostor");

        Optional<Impostor> impostor =  impostorService.getImpostorById(Long.parseLong(impostorId));

        if(impostor.isPresent()){
            AdditionalFormOptions opt = new AdditionalFormOptions();
            opt.setAlive(impostor.get().isAlive());
            opt.setEmail(impostor.get().getPlayer().getEmail());

            modelAndView.addObject("impostor", impostor.get());
            modelAndView.addObject("option", opt);
            modelAndView.addObject("gameId", gameId);
        }else {
            modelAndView.setViewName("redirect:" + GAME_ENDPOINT);
        }

        return modelAndView;
    }

    /* Edit a crewmate */
    @GetMapping("/crewmate/{crewmateId}/edit")
    public ModelAndView editGameCrewmate(
            @PathVariable(value="gameId") String gameId,
            @PathVariable(value="crewmateId") String crewmateId) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("game/role/addCrewmate");

        Optional<Crewmate> crewmate = crewmateService.getCrewMateById(Long.parseLong(crewmateId));

        if(crewmate.isPresent()){
            AdditionalFormOptions opt = new AdditionalFormOptions();
            opt.setAlive(crewmate.get().isAlive());
            opt.setEmail(crewmate.get().getPlayer().getEmail());

            modelAndView.addObject("crewmate", crewmate.get());
            modelAndView.addObject("option", opt);
            modelAndView.addObject("gameId", gameId);
        }else {
            modelAndView.setViewName("redirect:" + GAME_ENDPOINT);
        }

        return modelAndView;
    }

    /* Add a crewmate to game */
    @GetMapping("/crewmate/add")
    public ModelAndView addGameCrewmate(@PathVariable(value="gameId") String gameId) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("crewmate", new Crewmate());
        modelAndView.addObject("option", new AdditionalFormOptions());
        modelAndView.addObject("gameId", gameId);
        modelAndView.setViewName("game/role/addCrewmate");

        return modelAndView;
    }

    /* Save the impostor on DB */
    @PostMapping("/impostor")
    public ModelAndView saveGameRole(
            @PathVariable(value="gameId") String gameId,
            @ModelAttribute Impostor impostor,
            @ModelAttribute AdditionalFormOptions options
    ) {
        roleService.addGameRoleWithOptions(impostor, options, gameId);

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:" + GAME_ENDPOINT + "/" + gameId);

        return modelAndView;
    }

    /* Save the crewmate on DB */
    @PostMapping("/crewmate")
    public ModelAndView saveGameRole2(
            @PathVariable(value="gameId") String gameId,
            @ModelAttribute Crewmate crewmate,
            @ModelAttribute AdditionalFormOptions options
    ) {
        roleService.addGameRoleWithOptions(crewmate, options, gameId);

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:" + GAME_ENDPOINT + "/" + gameId);

        return modelAndView;
    }

    /* Delete an impostor */
    @GetMapping("/impostor/{impostorId}/delete")
    public ModelAndView deleteGameImpostor(
            @PathVariable(value="gameId") String gameId,
            @PathVariable(value="impostorId") String impostorId) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:" + GAME_ENDPOINT + "/" + gameId);

        impostorService.removeImpostorById(Long.parseLong(impostorId));

        return modelAndView;
    }

    /* Delete a crewmate */
    @GetMapping("/crewmate/{crewmateId}/delete")
    public ModelAndView deleteGameCrewmate(
            @PathVariable(value="gameId") String gameId,
            @PathVariable(value="crewmateId") String crewmateId) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:" + GAME_ENDPOINT + "/" + gameId);

        crewmateService.removeCrewmateById(Long.parseLong(crewmateId));

        return modelAndView;
    }
}
