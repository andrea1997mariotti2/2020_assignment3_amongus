package com.unimib.amongusrank.controller.game;

import com.unimib.amongusrank.model.*;
import com.unimib.amongusrank.service.game.GameService;
import com.unimib.amongusrank.service.game.role.RoleService;
import com.unimib.amongusrank.service.admin.MapService;
import com.unimib.amongusrank.service.login.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.text.ParseException;
import java.util.Optional;

import static com.unimib.amongusrank.controller.ControllerEndpoint.GAME_ENDPOINT;
@Controller
@RequestMapping(GAME_ENDPOINT)
public class GameController {

    @Autowired
    private GameService gameService;

    @Autowired
    private MapService mapService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private LoginService loginService;


    /*  Display all games */
    @GetMapping("")
    public ModelAndView getMatches(@CookieValue(name = "loggedPlayer", defaultValue = "-1") String loggedPlayer,
                                   @RequestParam(name = "gameData", required = false) String paramData,
                                   @RequestParam(name = "gameCode",required = false) String paramCode,
                                   @RequestParam(name = "searchEmail",required = false) String paramEmail
    ) throws ParseException {

        ModelAndView modelAndView = new ModelAndView();

        if(             (paramData != null && !paramData.equals(""))  ||
                (paramCode != null && !paramCode.equals(""))  ||
                (paramEmail != null && !paramEmail.equals(""))

        ){
            modelAndView.addObject("games", gameService.searchGame(paramCode, paramData, paramEmail));
        }else{
            modelAndView.addObject("games", gameService.getGames());
        }

        modelAndView.setViewName("game/games");
        modelAndView.addObject("playerPlayedGames", roleService.getRolesByPlayerId(Long.parseLong(loggedPlayer)));
        modelAndView.addObject("playerPlayedGames", roleService.getRolesByPlayerId(Long.parseLong(loggedPlayer)));


        return modelAndView;
    }

    /*  Create new Game */
    @GetMapping("/add")
    public ModelAndView getGame(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("game", new Game());
        modelAndView.addObject("maps", mapService.getMaps());
        modelAndView.addObject("option", new AdditionalFormOptions());
        modelAndView.setViewName("game/gameDetail");
        return modelAndView;
    }

    /*  Get info about single game   */
    @GetMapping("/{id}")
    public ModelAndView getSingleGame(
            @CookieValue(name = "loggedPlayer", defaultValue = "-1") String loggedPlayer,
            @PathVariable(value="id") String id
    ) {
        ModelAndView modelAndView = new ModelAndView();
        Optional<Game> game = gameService.getGameById(Long.parseLong(id));

        if(game.isPresent()){

            modelAndView.setViewName("game/gameRoles");
            modelAndView.addObject("game", game.get());
            modelAndView.addObject("roles",  roleService.getRolesByGame(game.get()));
            modelAndView.addObject("canAddRole", game.get().getSize() > gameService.getGameRolesCount(game.get()));
            modelAndView.addObject("gameId", id);
            modelAndView.addObject("playerRoles", roleService.getRolesByPlayerId(Long.parseLong(loggedPlayer)));
            modelAndView.addObject("playerAdmin", loginService.checkIfPlayerIsAdmin(Long.parseLong(loggedPlayer)));

            System.out.println(game.get().getSize() >= gameService.getGameRolesCount(game.get()));
            System.out.println(game.get().getSize());
            System.out.println(gameService.getGameRolesCount(game.get()));

        }else{
            modelAndView.setViewName("redirect:" + GAME_ENDPOINT);
        }

        return modelAndView;
    }

    /* Edit game details */
    @GetMapping("/{id}/edit")
    public ModelAndView editGame(@PathVariable(value="id") String id) {
        ModelAndView modelAndView = new ModelAndView();
        Optional<Game> game = gameService.getGameById(Long.parseLong(id));

        if(game.isPresent()){
            modelAndView.setViewName("game/gameDetail");
            modelAndView.addObject("game", game.get());
            modelAndView.addObject("maps", mapService.getMaps());
        }else {
            modelAndView.setViewName("redirect:" + GAME_ENDPOINT);
        }



        return modelAndView;
    }


    /*  Insert/Update the game */
    @PostMapping("/add")
    public ModelAndView updateGame(
            @CookieValue(name = "loggedPlayer", defaultValue = "-1") String loggedPlayer,
            @ModelAttribute Game game,
            @ModelAttribute AdditionalFormOptions options) {

        ModelAndView modelAndView = new ModelAndView();

        //Insert new game
        if(game.getId() == 0){
            Optional<Player> player = loginService.getPlayerById(Long.parseLong(loggedPlayer));
            if (player.isPresent()){
                Role playerRole;

                if(options.isImpostor()){
                    playerRole = new Impostor();
                }else{
                    playerRole = new Crewmate();
                }

                gameService.saveGame(game);

                playerRole.setRoleName(options.getRoleName());
                playerRole.setGame(game);
                playerRole.setPlayer(player.get());
                roleService.saveRole(playerRole);
            }

            modelAndView.setViewName("redirect:" + GAME_ENDPOINT);

        } else { //Update game

            gameService.saveGame(game);
            modelAndView.setViewName("redirect:" + GAME_ENDPOINT + "/" + game.getId());

        }

        return modelAndView;

    }
}