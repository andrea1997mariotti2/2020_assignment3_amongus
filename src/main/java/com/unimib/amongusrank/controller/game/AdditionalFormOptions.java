package com.unimib.amongusrank.controller.game;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public
class AdditionalFormOptions {

    private String email;
    private String roleName;
    private boolean alive;
    private boolean impostor;

}
