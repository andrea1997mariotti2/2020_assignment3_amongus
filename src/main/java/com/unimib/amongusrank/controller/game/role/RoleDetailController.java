package com.unimib.amongusrank.controller.game.role;

import com.unimib.amongusrank.model.Crewmate;
import com.unimib.amongusrank.model.Impostor;
import com.unimib.amongusrank.model.Role;
import com.unimib.amongusrank.model.Vote;
import com.unimib.amongusrank.model.*;
import com.unimib.amongusrank.service.admin.TaskService;
import com.unimib.amongusrank.service.game.role.*;
import com.unimib.amongusrank.service.game.role.task.CrewmateTaskService;
import com.unimib.amongusrank.service.game.role.vote.VoteService;
import com.unimib.amongusrank.service.login.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Optional;

import static com.unimib.amongusrank.controller.ControllerEndpoint.*;

@Controller
@RequestMapping(GAME_ROLE_ID_ENDPOINT)
public class RoleDetailController {

    @Autowired
    private VoteService voteService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private ImpostorService impostorService;

    @Autowired
    private CrewmateTaskService crewmateTaskService;

    @Autowired
    private CrewmateService crewmateService;

    @Autowired
    private TaskService taskService;

    @Autowired
    private LoginService loginService;



    @GetMapping("")
    public ModelAndView showRole(
            @PathVariable(value="roleId") String id,
            @PathVariable(value="gameId") String gameId,
            @CookieValue(name = "loggedPlayer", defaultValue = "-1") String loggedPlayer

    ){
        ModelAndView modelAndView = new ModelAndView();
        long longId=Long.parseLong(id);

        boolean isImpostor = impostorService.isImpostor(id);
        if (isImpostor){
            Optional<Impostor> impostorOpt = impostorService.getImpostorById(longId);
            if(impostorOpt.isPresent()){
                Impostor impostor = impostorOpt.get();

                modelAndView.addObject("role", impostor);
                modelAndView.addObject("votes", voteService.getVotesByRole(impostor));
                modelAndView.addObject("impostor",true);
                modelAndView.addObject("roles", roleService.getRolesByGame(impostor.getGame()));
                modelAndView.addObject("modifiable",
                        roleService.verifyRolePayerID(loggedPlayer,impostor) ||
                                loginService.checkIfPlayerIsAdmin(Long.parseLong(loggedPlayer)));
            } else {
                modelAndView.setViewName("redirect:" + GAME_ENDPOINT + "/" + gameId);
            }

        }else{
            Optional<Crewmate> crewmateOpt = crewmateService.getCrewMateById(longId);
            if(crewmateOpt.isPresent()) {

                Crewmate crewmate = crewmateOpt.get();

                modelAndView.addObject("role", crewmate);
                modelAndView.addObject("votes", voteService.getVotesByRole(crewmate));
                modelAndView.addObject("tasks", crewmateTaskService.getAllCrewmateTasks(crewmate));
                modelAndView.addObject("impostor", false);
                modelAndView.addObject("roles", roleService.getRolesByGame(crewmate.getGame()));

                modelAndView.addObject("newVote", new Vote(crewmate, null));

                modelAndView.addObject("avaiableTasks", taskService.getAllTasks());
                modelAndView.addObject("crewmateTask", new CrewmateTask());
                modelAndView.addObject("modifiable",
                        roleService.verifyRolePayerID(loggedPlayer, crewmate) ||
                                loginService.checkIfPlayerIsAdmin(Long.parseLong(loggedPlayer)));
            } else {
                modelAndView.setViewName("redirect:" + GAME_ENDPOINT + "/" + gameId);
            }
        }
        modelAndView.addObject("newVote", new Vote());
        modelAndView.addObject("roleId", id);
        modelAndView.addObject("gameId", gameId);

        modelAndView.setViewName("game/role/role");
        return modelAndView;
    }


    @PostMapping("/addVote")
    public ModelAndView addVote(
            @PathVariable(value="roleId") String id,
            @ModelAttribute Vote vote){

        ModelAndView modelAndView = new ModelAndView();

        Optional<Role> role = roleService.getRoleById(Long.parseLong(id));
        if(role.isPresent()){
            vote.setVoteFrom(role.get());
            voteService.addVote(vote);
        }

        modelAndView.setViewName("redirect:" + GAME_ROLE_ENDPOINT + "/" + id);
        return modelAndView;
    }

    @PostMapping("/addTask")
    public ModelAndView addTask(
            @PathVariable(value="roleId") String id,
            @ModelAttribute CrewmateTask crewmateTask
    ) {
        ModelAndView modelAndView = new ModelAndView();

        Optional<Crewmate> crewmate = crewmateService.getCrewMateById(Long.parseLong(id));
        if (crewmate.isPresent()){
            crewmateTask.setCrewmate(crewmate.get());
            crewmateTaskService.saveCTask(crewmateTask);
        }


        modelAndView.setViewName("redirect:" + GAME_ROLE_ENDPOINT + "/" + id);
        return modelAndView;
    }

    @GetMapping("/removeTask/{crewmateTaskId}")
    public ModelAndView removeTask(
            @PathVariable(value="roleId") String roleId,
            @PathVariable(value="crewmateTaskId") String crewmateTaskId
    ) {
        ModelAndView modelAndView = new ModelAndView();

        crewmateTaskService.removeCrewmateTask(Long.parseLong(crewmateTaskId));

        modelAndView.setViewName("redirect:" + GAME_ROLE_ENDPOINT + "/" + roleId);
        return modelAndView;
    }

    @GetMapping("/removeVote/{voteId}")
    public ModelAndView removeVote(
            @PathVariable(value="roleId") String roleId,
            @PathVariable(value="voteId") String voteId
    ) {
        ModelAndView modelAndView = new ModelAndView();

        voteService.removeVoteById(Long.parseLong(voteId));

        modelAndView.setViewName("redirect:" + GAME_ROLE_ENDPOINT + "/" + roleId);
        return modelAndView;
    }


}
