package com.unimib.amongusrank.controller.register;

import com.unimib.amongusrank.model.Player;
import com.unimib.amongusrank.service.login.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import static com.unimib.amongusrank.controller.ControllerEndpoint.*;

@Controller
@RequestMapping(REGISTER_ENDPOINT)
public class RegisterController {

    @Autowired
    private LoginService service;

    @GetMapping("")
    public ModelAndView signUpPlayer(@ModelAttribute Player player) {

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("player", new Player());
        modelAndView.setViewName("register/register");

        return modelAndView;
    }

    @PostMapping("")
    public ModelAndView signUpControlPlayer(@ModelAttribute Player player, HttpServletResponse response) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("player", new Player());

        if(service.isEmailAlreadyExisting(player.getEmail())){
            service.modifyPasswordByEmail(player.getEmail(), player.getPassword());
        }else{
            service.addPlayer(player);
        }

        player.setId(service.getIdByEmail(player.getEmail()));
        modelAndView.setViewName("redirect:" + GAME_ENDPOINT);
        Cookie cookie = new Cookie("loggedPlayer", String.valueOf(player.getId()));
        cookie.setPath("/");
        response.addCookie(cookie);

        return modelAndView;
    }
}
