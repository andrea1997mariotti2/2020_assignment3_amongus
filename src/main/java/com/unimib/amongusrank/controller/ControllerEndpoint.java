package com.unimib.amongusrank.controller;

public class ControllerEndpoint {

    public final static String ADMIN_ENDPOINT = "/admin";
    public final static String ADMIN_MAP_ENDPOINT = ADMIN_ENDPOINT + "/maps";
    public final static String ADMIN_TASK_ENDPOINT = ADMIN_ENDPOINT + "/tasks";

    public final static String GAME_ENDPOINT = "/games";
    public final static String GAME_ID_ENDPOINT = GAME_ENDPOINT + "/{gameId}";
    public final static String GAME_ROLE_ENDPOINT = GAME_ID_ENDPOINT + "/role";
    public final static String GAME_ROLE_ID_ENDPOINT = GAME_ROLE_ENDPOINT + "/{roleId}";

    public final static String LOGIN_ENDPOINT = "/login";
    public final static String REGISTER_ENDPOINT = "/register";


}
