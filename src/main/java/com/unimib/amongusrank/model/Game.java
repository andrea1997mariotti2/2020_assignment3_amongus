package com.unimib.amongusrank.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
@Getter
@Setter
public class Game implements Serializable{

    private static final long serialVersionUID = -5124436115031696628L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "game_id", unique = true)
    private long id;

    @Temporal(TemporalType.DATE)
    @Column(name = "game_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date date;

    @Column
    private String code;

    @ManyToOne
    private Map map;

    @Column
    private int size;

    @Override
    public String toString() {
        return "Game{" +
                "id=" + id +
                ", date=" + date +
                ", code='" + code + '\'' +
                ", map=" + map.getId() +
                '}';
    }
}
