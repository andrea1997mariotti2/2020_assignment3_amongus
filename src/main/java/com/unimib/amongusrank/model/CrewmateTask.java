package com.unimib.amongusrank.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class CrewmateTask implements Serializable {

    private static final long serialVersionUID = -7303567816415032187L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "crewmate_task_id", updatable = false, nullable = false)
    private long id;

    @ManyToOne
    private Crewmate crewmate;

    @ManyToOne
    private Task task;

    @Column
    @Temporal(TemporalType.TIME)
    @DateTimeFormat(pattern = "mm:ss")
    private Date completionTime;

}
