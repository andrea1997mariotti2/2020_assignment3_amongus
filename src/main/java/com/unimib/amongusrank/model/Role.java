package com.unimib.amongusrank.model;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@Inheritance(strategy = InheritanceType.JOINED)
public class Role implements Serializable {

    private static final long serialVersionUID = -7708568737030987833L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "role_id", updatable = false, nullable = false)
    protected long id;

    @Column(name = "role_name", nullable = false)
    private String roleName;

    @Column(name = "alive", nullable = true)
    private boolean alive;

    @ManyToOne
    private Player player;

    @ManyToOne
    private Game game;

    protected Role() { }

}