package com.unimib.amongusrank.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.type.descriptor.sql.TinyIntTypeDescriptor;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class Task implements Serializable{

    private static final long serialVersionUID = -6204278621792573047L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "task_id",unique = true)
    private long id;

    @Column(name = "task_name",nullable = true)
    private String name;

    @Column( nullable = true)
    private Boolean visual;

    public Task(long taskId, String taskName, Boolean visual) {
        this.id = taskId;
        this.name = taskName;
        this.visual = visual;
    }

    public Task() {
    }
}
