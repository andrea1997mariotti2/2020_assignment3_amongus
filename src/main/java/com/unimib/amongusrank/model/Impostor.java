package com.unimib.amongusrank.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@ToString
@Getter
@Setter
public class Impostor extends Role {

    @Column(name = "kills", nullable = true)
    private int kills;

    @Column(name = "sabotages", nullable = true)
    private int sabotages;

    public Impostor() {

    }

}
