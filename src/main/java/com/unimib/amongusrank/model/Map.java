package com.unimib.amongusrank.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@ToString
@Getter
@Setter
public class Map implements Serializable {

    private static final long serialVersionUID = 1196133073540074126L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "map_id", unique = true)
    private long id;

    @Column(name = "map_name", nullable = true)
    private String mapName;

    @Column(name = "max_size", nullable = true)
    private int maxSize;


    public Map(){ }

    public Map(long id, String mapName, int maxSize) {
        this.id = id;
        this.mapName = mapName;
        this.maxSize = maxSize;
    }

}
