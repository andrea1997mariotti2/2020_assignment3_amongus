package com.unimib.amongusrank.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

@Entity
@ToString
@Getter
@Setter
@NoArgsConstructor
public class Player implements Serializable {

    private static final long serialVersionUID = -5124436115031696628L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "player_id", unique = true)
    private long id;

    @Column(name = "email", nullable = true)
    private String email;

    @Column(name = "password", nullable = true)
    private String password;

    @Column(name = "isAdmin", nullable = true)
    private boolean isAdmin;

}
