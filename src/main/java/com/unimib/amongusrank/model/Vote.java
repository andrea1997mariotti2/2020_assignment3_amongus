package com.unimib.amongusrank.model;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@Setter
public class Vote implements Serializable {

    private static final long serialVersionUID = 8132739305644128062L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "vote_id", updatable = false, nullable = false)
    protected long id;

    @ManyToOne
    private Role voteFrom;

    @ManyToOne
    private Role voteTo;

    private long count;


    public Vote() { }

    public Vote(Role voteFrom, Role voteTo) {
        this.voteFrom = voteFrom;
        this.voteTo = voteTo;
        this.count=0;
    }
}