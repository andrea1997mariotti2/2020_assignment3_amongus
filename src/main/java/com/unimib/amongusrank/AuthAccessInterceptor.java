package com.unimib.amongusrank;


import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

@Component
public class AuthAccessInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle
            (HttpServletRequest request,
             HttpServletResponse response,
             Object handler
            )
            throws Exception {

        if(request.getCookies() != null){
            Cookie playerCookie = WebUtils.getCookie(request, "loggedPlayer");
            String playerId =  playerCookie != null ? playerCookie.getValue() : "";

            /*
            * CONTROL LOGIC TO CHECK IF PLAYER IS AUTHENTICATED
            * */

            if(!playerId.equals("-1")){
                return true;
            }
        }

        response.sendRedirect("/login");
        return false;


    }
}